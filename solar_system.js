"use strict"

//--------------------------------------------------------------------------------------------------------
// GLSL
//--------------------------------------------------------------------------------------------------------

// Shader to display a fullscreen quad
var vertexRenderGlow =
  `#version 300 es

// OUTPUT
// Texture coordinates
out vec2 texCoord;

void main()
{
  // Compute vertex position between [-1;1]
  float x = -1.0 + float( ( gl_VertexID & 1 ) << 2 );	// If VertexID == 1 then x = 3 else x == -1
  float y = -1.0 + float( ( gl_VertexID & 2 ) << 1 ); // If VertexID == 2 then y = 3 else y == -1

  // Compute texture coordinates between [0;1] (-1 * 0.5 + 0.5 = 0 and 1 * 0.5 + 0.5 = 1)
  texCoord.x = (x + 1.0) * 0.5;
  texCoord.y = (y + 1.0) * 0.5;

  // Send position to clip space
  gl_Position = vec4( x, y, 0.0, 1.0 );
}
`;

// Shader to draw the glow framebuffer
var fragRenderGlow =
  `#version 300 es
precision highp float;

// INPUT
// Texture coordinates
in vec2 texCoord;

// UNIFORMS
uniform sampler2D uSampler;

// OUTPUT
out vec4 oColor;

void main()
{
  vec4 color;
  color = texture(uSampler, texCoord);
  oColor = color;
}
`;


// Shader made to apply the gaussian blur to a framebuffer
var fragGlowBlur =
  `#version 300 es
precision highp float;

in vec2 texCoord;

uniform sampler2D uSampler;
uniform vec2 uSize;
uniform int uVertical;

out vec4 oColor;

float kernel[3] = float[](0.335568, 0.241812, 0.090404);

vec2 p2t(vec2 fc) {
  return fc / uSize;
}

vec4 computeBlur(int vertical) {
  vec4 color = texture(uSampler, texCoord) * kernel[0];
  for (int i = 1; i < 3; i++) {
    float coordX = vertical == 0 ? float(i) : 0.;
    float coordY = vertical == 1 ? float(i) : 0.;
    vec2 textureCoord = p2t(vec2(gl_FragCoord.x + coordX, gl_FragCoord.y + coordY));
    if (!(textureCoord.x < 0. || textureCoord.y < 0. || textureCoord.x > 1. ||
      textureCoord.y > 1.))
      color += texture(uSampler, textureCoord) * kernel[i];

    textureCoord = p2t(vec2(gl_FragCoord.x - coordX, gl_FragCoord.y - coordY));
    if (!(textureCoord.x < 0. || textureCoord.y < 0. || textureCoord.x > 1. ||
      textureCoord.y > 1.))
      color += texture(uSampler, textureCoord) * kernel[i];
  }
  return color;
}

void main() {
  vec4 color;
  color = computeBlur(uVertical);
  oColor = color;
}

`

// Shader to display a unique color in order to apply a blur later.
var fragGlow =
  `#version 300 es
precision highp float;

out vec4 oColor;

uniform vec4 uGlowColor;
uniform float uGlowIntensity;

void main() {
  oColor = uGlowColor * uGlowIntensity;
}
`;

// Vertex shader for the skybox
var vertSkyBox =
  `#version 300 es

layout(location = 1) in vec3 position_in;

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;
//uniform mat4 uModelMatrix;

out vec3 v_textureCoord;

void main () {
  gl_PointSize = 10.0;
  v_textureCoord = position_in;

  gl_Position = uProjectionMatrix * uViewMatrix  *  vec4(position_in, 1.0);
}
`;

// Fragment shader for the skybox
var fragSkyBox =
  `#version 300 es
precision highp float;

in vec3 v_textureCoord;

out vec4 oFragmentColor;

uniform samplerCube uSampler;

void main () {
  vec4 textureColor = texture(uSampler, v_textureCoord);
  oFragmentColor = textureColor;
}
`;

// Main vertex shader (used for all planets and the sun)
var vertBody =
  `#version 300 es
layout(location = 1) in vec3 position_in;
layout(location = 2) in vec3 normal_in;
layout(location = 3) in vec2 textureCoord_in;

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uModelMatrix;
uniform mat3 normalMatrix;
uniform vec3 lightSource;

out vec2 v_textureCoord;
out vec3 v_normal;
out vec3 v_position;
out vec3 v_lightSource;

void main () {
  gl_PointSize = 10.0;
  v_textureCoord = textureCoord_in;

  v_normal = normalMatrix * normal_in;
  v_lightSource = lightSource; //vec3(uProjectionMatrix * uViewMatrix * vec4(lightSource, 1.0));
  v_position = vec3(uModelMatrix * vec4(position_in, 1.0));
  gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(position_in, 1.0);
}
`;

var fragBody =
  `#version 300 es
precision highp float;

in vec2 v_textureCoord;
in vec3 v_normal;
in vec3 v_position;
in vec3 v_lightSource;

out vec4 oFragmentColor;

// Texture
uniform sampler2D uSampler;

// Eartg
uniform sampler2D uSamplerClouds;
uniform sampler2D uSamplerNight;
uniform bool isEarth;
uniform float time;

// Light
uniform vec3 lightIntensity;
uniform vec3 ka;
uniform vec3 ks;
uniform bool isSun;

void main () {
  vec3 lightDir = normalize(v_lightSource - v_position);
  vec4 textureColor;
  vec3 normal = normalize(v_normal);
  float exposure = max(dot(normal, lightDir), 0.0);

  vec3 id;
  vec3 is;
  vec3 ia;

  // If we're treating the earth, we want to add the night texture and clouds
  if (isEarth) {
    vec4 day_night = mix(texture(uSamplerNight, v_textureCoord),
      texture(uSampler, v_textureCoord),
      clamp(exposure, 0.0, 1.0)
    );
    textureColor = mix(day_night,
      texture(uSamplerClouds,
        vec2(v_textureCoord.x + (time / 3.) , v_textureCoord.y)) * clamp(exposure, 0.05, 1.0)
      , 0.6);
  }
  else {
    textureColor = texture(uSampler, v_textureCoord);
  }
  ia = lightIntensity * ka;

  // We don't want any light calculations on the sun because it's the light source
  if (isSun)
    id = lightIntensity * textureColor.rgb;
  else
    id = isEarth ? textureColor.rgb : lightIntensity * textureColor.rgb * exposure;
  is = ks * vec3(0., 0., 0.);

  // No specular color here because it doesn't make much sense for planets.
  oFragmentColor = vec4(ia + id + is, 1.0);
}
`;

// Vertex shader for the asteroids
var vertRock =
  `#version 300 es
layout(location = 0) in vec3 position_in;
layout(location = 2) in vec2 textureCoord_in;
layout(location = 3) in mat4 instanceMatrix;

uniform mat4 uProjectionMatrix;
uniform mat4 uViewMatrix;

out vec2 v_textureCoord;

void main () {
  gl_PointSize = 10.0;
  v_textureCoord = textureCoord_in;

  gl_Position = uProjectionMatrix * uViewMatrix * instanceMatrix * vec4(position_in, 1.0);
}
`;

// Fragment shader for the asteroids
var fragRock =
  `#version 300 es
precision highp float;

in vec2 v_textureCoord;

out vec4 oFragmentColor;

uniform sampler2D uSampler;

void main () {
  vec4 textureColor = texture(uSampler, v_textureCoord);
  oFragmentColor = textureColor;
}
`
//--------------------------------------------------------------------------------------------------------

// Global variables : textures, FBOs, prog_shaders, mesh, renderer, and a lot of parameters
// Time and time speed. Custom so speed time changes can be smooth.


// This is a custom time variable, used to slow down or accelerate time.
// It is used by the clouds on earth, and by the rotation and revolution of
// every planet.
var t = 0;

// The speed of time can be changed by the user
var timeSpeed = 1;

window.setInterval(function(){
  t += 0.001 * timeSpeed;
}, 1);

// Skybox
var prgSkyBox = null;
var skyboxRend = null;
var texSkybox = null;

// Bodies
const sunSize = 109.12;
const maxDist = 4400 * 2;
var prgBody = null;
var wireframe = false;

// Light
var lightPower = 1;
var lightIntensity = Vec3(lightPower, lightPower, lightPower);
var ka = Vec3(0.0, 0.0, 0.0);
var ks = Vec3(1.0, 1.0, 1.0);

// Rocks
var prgRocks = null;
var rockRend = null;
var texRock = null;
const nbAsteroids = 100000;
const rockDist = 500 + sunSize / 2;
var rockInstance = 1000;
var rockWidth = 50;
var rockDepth = 20;

var bodyRend = null;

// This is the main array that contains all information about all bodies.
// Those caracteristics are read in the main loop in order to render each
// body correctly.
var bodyCaracs = [
  { name: "Sun", tex: null, size: sunSize, angle : 0, dist: 0, per: 0, sidper: 25.38, glow: Vec4(0.5, 0.2, 0.0, 1.0), glowSize: 1.1, glowPasses: 100 },
  { name: "Mercury", tex: null, size: 0.38, angle: 0.1, dist: 60, per: 88, sidper: 175.9 },
  { name: "Venus", tex: null, size: 0.95, angle: 177, dist: 120, per: 224.7, sidper: 116.7 },
  { name: "Earth", tex: null, size: 1, angle: 23, dist: 150, per: 365.25, sidper: 1.003, glow: Vec4(0.2, 0.3, 0.6, 0.1), glowSize: 1.02, glowPasses: 20 },
  { name: "Mars", tex: null, size: 0.53, angle: 25, dist: 230, per: 689.0, sidper: 1.03 },
  { name: "Jupiter", tex: null, size: 11.21, angle: 3, dist: 800, per: 11.87 * 365.25, sidper: 9.92 / 24 },
  { name: "Saturn", tex: null, size: 9.45, angle: 27, dist: 1400, per: 29.45 * 365.25, sidper: 10.65 / 24},
  { name: "Uranus", tex: null, size: 4.02, angle: 98, dist: 3000, per: 84.07 * 365.25, sidper: 17.24 / 24 },
  { name: "Neptune", tex: null, size: 3.91, angle: 30, dist: 4400, per: 164.89 * 365.25, sidper: 16.11 / 24 },
];

// Augment some of the caracteristics or translate them for the program
bodyCaracs.forEach((e, i, a) => {
  a[i].dist = e.dist ? e.dist + sunSize/2 : 0;
  a[i].speed = e.per ? (Math.PI*2)/e.per : 0;
  a[i].rotSpeed = e.sidper ? 360/e.sidper : 0;
  a[i].posX = e.dist;
  a[i].posY = 0;
});

// List of the different textures. Order is important here
var bodyTexPaths = [
  "/images/2k_sun.jpg",
  "/images/2k_mercury.jpg",
  "/images/2k_venus.jpg",
  "/images/2k_earth_daymap.jpg",
  "/images/2k_mars.jpg",
  "/images/2k_jupiter.jpg",
  "/images/2k_saturn.jpg",
  "/images/2k_uranus.jpg",
  "/images/2k_neptune.jpg"
]

// Currently selected body (used for the user to change body)
var selectedBody = 0

// Glow
var prgGlow = null;
var prgBlur = null;
var prgRenderGlow = null;
var texGlow1 = null;
var texGlow2 = null;
var fboGlow1 = null;
var fboGlow2 = null;

const fboWidth = 300;
const fboHeight = 100;

function init_wgl()
{
  ewgl.continuous_update = true;
  gl.blendFunc(gl.ONE, gl.ONE);

  // Generate all the textures
  let tex = Texture2d();
  tex.load("/images",gl.RGB8);
  // ...

  // texture cubeMap for the skybox
  texSkybox = TextureCubeMap([gl.TEXTURE_MIN_FILTER, gl.LINEAR]);
  texSkybox.load(["/images/skybox/skybox_1k.png",
    "/images/skybox/skybox_1k.png",
    "/images/skybox/skybox_1k.png",
    "/images/skybox/skybox_1k.png",
    "/images/skybox/skybox_1k.png",
    "/images/skybox/skybox_1k.png",])
    .then( () => {
      update_wgl();
    });

  // Initialize every texture
  bodyTexPaths.forEach((el, i) => {
    let tex = Texture2d([gl.TEXTURE_MIN_FILTER, gl.LINEAR],
      [gl.TEXTURE_MAG_FILTER, gl.LINEAR],
      [gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST]);
    tex.load(el).then(() => update_wgl);
    bodyCaracs[i].tex = tex;
    if (i == 3) {
      // Load cloud texture for the earth
      let cloud = Texture2d([gl.TEXTURE_MIN_FILTER, gl.LINEAR],
        [gl.TEXTURE_MAG_FILTER, gl.NEAREST],
        [gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST],
        [gl.TEXTURE_WRAP_S, gl.REPEAT]);
      cloud.load("/images/2k_earth_clouds.jpg").then(update_wgl);
      bodyCaracs[i].cloudTex = cloud;

      // Load night texture for the earth
      let night = Texture2d([gl.TEXTURE_MIN_FILTER, gl.LINEAR],
        [gl.TEXTURE_MAG_FILTER, gl.LINEAR],
        [gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST],
        [gl.TEXTURE_WRAP_S, gl.REPEAT]);
      night.load("/images/2k_earth_nightmap.jpg").then(update_wgl);
      bodyCaracs[i].nightTex = night;
    }
  });

  // Create a mesh cube and his renderer
  let skybox = Mesh.Cube();
  skyboxRend = skybox.renderer(1, 2, 3, 4, 5);

  // Create all the shader programs for skybox and bodies
  prgSkyBox = ShaderProgram(vertSkyBox, fragSkyBox);
  prgBody = ShaderProgram(vertBody, fragBody);

  // Create a mesh of a sphere and a renderer
  let body = Mesh.Sphere(50);
  bodyRend = body.renderer(1, 2, 3, 4, 5);

  // Set the radius and the center of the scene
  ewgl.scene_camera.set_scene_radius(body.BB.radius);
  ewgl.scene_camera.set_scene_center(body.BB.center);

  // Asteroid Belt
  // ----------------------------------------------------------------------------------------------------

  // Shader Program for asteroÃ¯ds
  prgRocks = ShaderProgram(vertRock, fragRock);

  // Asteroid texture
  texRock = Texture2d();
  texRock.load("/rock/rock.png").then(update_wgl);

  // Create a typed array to contain all the 4x4 model matrices of each asteroÃ¯d
  const matrixData = new Float32Array(4 * 4 * nbAsteroids);
  // For each asteroÃ¯d
  pause_wgl();
  for (let i = 0; i < nbAsteroids; ++i)
  {
    // Randomize everything !
    let randomPos = Math.random();
    let randomWidth = Math.random() * rockWidth;
    let randomSize = Math.random() / 2;
    // Randomize position around the circle
    let posX = (rockDist + randomWidth) * Math.cos(randomPos * Math.PI * 2);
    let posY = (rockDist + randomWidth) * Math.sin(randomPos * Math.PI * 2);
    var model = Matrix.mult(
      Matrix.translate(posX, Math.random() * rockDepth, posY),
      Matrix.rotateX(Math.random() * 360),
      Matrix.rotateY(Math.random() * 360),
      Matrix.rotateZ(Math.random() * 360),
      Matrix.scale(randomSize, randomSize, randomSize),
    );

    // Put the matrix model a the right place in the typed array
    var index = 16 * i;
    matrixData.set(model.data, index);
  }

  // VBO for model matrix of each instance
  const matrixBuffer = VBO(matrixData);

  // Load the .obj mesh and use an instanced renderer (with 4 VBO, to recreate a 4x4 matrix) to get a lot of asteroÃ¯ds
  Mesh.loadObjFile("/rock/rock.obj").then((meshes) =>
    {
      rockRend = meshes[0].instanced_renderer([
        [3, matrixBuffer, 1, 4 * 4, 0 * 4, 4],
        [4, matrixBuffer, 1, 4 * 4, 1 * 4, 4],
        [5, matrixBuffer, 1, 4 * 4, 2 * 4, 4],
        [6, matrixBuffer, 1, 4 * 4, 3 * 4, 4]],
        0, 1, 2);
      update_wgl();
    });
  // then, the matrice of an instance can be retrieved in a vertex shader with : layout(location=3) in mat4 instanceMatrix;
  // ----------------------------------------------------------------------------------------------------

  // ATMOSPHERE (GLOW)
  // Create Shader programs ...
  // Create FBOs with the linked textures ... 
  fboGlow1 = gl.createFramebuffer();
  fboGlow2 = gl.createFramebuffer();
  prgGlow = ShaderProgram(vertBody, fragGlow);
  prgBlur = ShaderProgram(vertexRenderGlow, fragGlowBlur);
  prgRenderGlow = ShaderProgram(vertexRenderGlow, fragRenderGlow);

  texGlow1 = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texGlow1);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, fboWidth, Math.floor(fboWidth * (9/16)),
    0, gl.RGBA, gl.UNSIGNED_BYTE, null);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.bindTexture(gl.TEXTURE_2D, null);
  texGlow2 = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texGlow2);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, fboWidth, Math.floor(fboWidth * (9/16)),
    0, gl.RGBA, gl.UNSIGNED_BYTE, null);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.bindTexture(gl.TEXTURE_2D, null);

  gl.bindFramebuffer(gl.FRAMEBUFFER, fboGlow1);
  gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texGlow1, 0);
  gl.drawBuffers([gl.COLOR_ATTACHMENT0]);
  gl.bindFramebuffer(gl.FRAMEBUFFER, fboGlow2);
  gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texGlow2, 0);
  gl.drawBuffers([gl.COLOR_ATTACHMENT0]);
  gl.bindFramebuffer(gl.FRAMEBUFFER, null);

  // User interface
  UserInterface.begin("Interface", [true], [true]);
  let s1 = UserInterface.add_slider("Time Scale", 0, 1000, 10, (val) => {
    timeSpeed = val/10;
  }, val => val/10, 1);
  let s2 = UserInterface.add_slider("Number of asteroids", 0, 100000, 1000,
    (val) => { rockInstance = val}, val => val, 0);
  let s3 = UserInterface.add_slider("Light Intensity", 0, 1000, 10, (val) => {
    lightPower = val/10;
    lightIntensity = Vec3(lightPower, lightPower, lightPower);
  }, val => val/10, 1);
  let l1 = UserInterface.add_list_input(bodyCaracs.map(e => e.name), 0, (i) => {
    selectedBody = i;
  });
  let cb = UserInterface.add_check_box("Wireframe", false, (val) => {
    wireframe = val;
  });
  UserInterface.end();
}

function getRandomMax(max)
{
  return Math.random() * Math.floor(max);
}

function getRandomMinMax(min, max)
{
  return Math.random() * (max - min) + min;
}

/* function resize_wgl(w,h)
{
    let d = Math.pow(2, 3);
  //
  fbo1.resize(w/d,h/d);
  fbo2.resize(w/d,h/d);
    // Faire varier l'intensiter selon la taille
    // glow_intensity = 300 - ((w/100) * (h/100));
} */

    // -------------------------------------------------------------------------------------------------------------------------------------
    //  DRAW
    // -------------------------------------------------------------------------------------------------------------------------------------
function draw_wgl()
{
  gl.clearColor(0,0,0,0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  // Disable depth depth for the skybox (avoid the skybox hiding other objects)
  gl.disable(gl.DEPTH_TEST);

  // Compute new positions
  pause_wgl();
  bodyCaracs.forEach((b, i, a) => {
    a[i].posX = b.dist * Math.cos(b.speed * t);
    a[i].posY = b.dist * Math.sin(b.speed * t);
  });
  update_wgl();

  // set scene center according to the selected object
  let posX = bodyCaracs[selectedBody].posX;
  let posY = bodyCaracs[selectedBody].posY;
  let size = bodyCaracs[selectedBody].size;
  ewgl.scene_camera.set_scene_center(Vec3(posX, 0, posY));
  ewgl.scene_camera.set_scene_radius(maxDist * 2);

  // Compute the matrices (model - view - projection)
  let projMat = ewgl.scene_camera.get_projection_matrix();
  let viewMat = ewgl.scene_camera.get_view_matrix();

  prgSkyBox.bind();
  Uniforms.uSampler = texSkybox.bind(0);
  Uniforms.uProjectionMatrix = ewgl.scene_camera.get_projection_matrix_for_skybox();
  Uniforms.uViewMatrix = ewgl.scene_camera.get_view_matrix_for_skybox();

  // Render skybox
  skyboxRend.draw(gl.TRIANGLES);
  unbind_texture_cube();
  unbind_shader();

  gl.enable(gl.DEPTH_TEST);


  // Render Bodies
  let prim = wireframe ? gl.LINES : gl.TRIANGLES
  bodyCaracs.forEach((b, i, a) => {
    let modelMat = Matrix.mult(
      Matrix.translate(b.posX, 0, b.posY),
      Matrix.rotateX(b.angle - 90),
      Matrix.rotateZ(b.rotSpeed * t),
      Matrix.scale(b.size, b.size, b.size));
    // Glow
    if (b.glow) {
      gl.bindFramebuffer(gl.FRAMEBUFFER, fboGlow1);
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
      gl.viewport(0, 0, fboWidth, Math.floor(fboWidth * (9/16)));
      prgGlow.bind();
      // We Want the glow to be slightly bigger than the object
      Uniforms.uModelMatrix = Matrix.mult(modelMat, Matrix.scale(b.glowSize, b.glowSize, b.glowSize));
      Uniforms.uViewMatrix = viewMat;
      Uniforms.uProjectionMatrix = projMat;
      Uniforms.uGlowColor = b.glow;
      Uniforms.uGlowIntensity = 1.;
      bodyRend.draw(prim);
      prgBlur.bind();
      // Ping pong
      for (let i = 0; i < b.glowPasses; i++) {
        gl.bindFramebuffer(gl.FRAMEBUFFER, i%2 == 0 ? fboGlow2 : fboGlow1);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, i%2 == 0? texGlow1 : texGlow2);
        Uniforms.uSampler = 0;
        Uniforms.uSize = Vec2(fboWidth, Math.floor(fboWidth * (9/16)));
        Uniforms.uVertical = i%2;
        gl.drawArrays(gl.TRIANGLES, 0, 3);
        gl.bindTexture(gl.TEXTURE_2D, null);
      }
      gl.bindFramebuffer(gl.FRAMEBUFFER, null);
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
      // Render glow
      gl.enable(gl.BLEND);
      gl.disable(gl.DEPTH_TEST);
      prgRenderGlow.bind();
      gl.activeTexture(gl.TEXTURE0);
      gl.bindTexture(gl.TEXTURE_2D, texGlow2);
      Uniforms.uSampler = 0;
      gl.drawArrays(gl.TRIANGLES, 0, 3);
      gl.bindTexture(gl.TEXTURE_2D, null);
      gl.disable(gl.BLEND);
      gl.enable(gl.DEPTH_TEST);
      unbind_shader();
    }
    prgBody.bind();
    Uniforms.uModelMatrix = modelMat;
    Uniforms.uProjectionMatrix = projMat;
    Uniforms.uViewMatrix = viewMat;
    Uniforms.uSampler = b.tex.bind(0);
    Uniforms.isEarth = false;
    Uniforms.isSun = false;
    Uniforms.time = t;

    // Light
    Uniforms.ka = ka;
    // Uniforms.ks = ks;
    Uniforms.lightSource = Vec3(0.0, 0.0, 0.0);
    Uniforms.lightIntensity = lightIntensity;
    let mvm = Matrix.mult(modelMat);
    Uniforms.normalMatrix = mvm.inverse3transpose();
    // If Sun
    if (i == 0) {
      Uniforms.isSun = true;
    }
    // If Earth
    if (i == 3) {
      Uniforms.uSamplerClouds = b.cloudTex.bind(1);
      Uniforms.uSamplerNight = b.nightTex.bind(2);
      Uniforms.isEarth = true;
    }
    bodyRend.draw(prim);
    unbind_texture2d();
  });
  unbind_shader();


  // Render asteroÃ¯ds
  prgRocks.bind();
  Uniforms.uSampler = texRock.bind(0);
  Uniforms.uProjectionMatrix = projMat;
  Uniforms.uViewMatrix = viewMat;
  if (rockRend) rockRend.draw(gl.TRIANGLES, rockInstance);
  unbind_texture2d();
  unbind_shader();
}

function mousedown_wgl(ev)
{
  // if you want to use mouse interaction
}

function onkeydown_wgl(k)
{
  // if you want to use keyboard interaction
}

ewgl.launch_3d();
